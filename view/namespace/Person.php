<?php
/**
 * Created by PhpStorm.
 * User: OLINE
 * Date: 2019-03-26
 * Time: 11:01 AM
 */
namespace App;
class Person{
    public $name="Salabir";
    public $gender="Male";
    public $blood_group="O+";

    public function showPersonInfo(){
        echo $this->name."<br/>";
        echo $this->gender."<br/>";
        echo $this->blood_group."<br/>";
    }
}