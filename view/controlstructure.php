<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Control Structure</title>
</head>
<body>
<form action="" method="post">
    Enter your code:<input type="text" name="switch"> <br>
    <input type="submit" value="submit">
</form>
<?php
if(isset($_POST['switch'])){
    $a=$_POST['switch'];
    switch($a){
        case 0:
            echo "The value is 0";
            break;
        case 1:
            echo "The value is 1";
            break;
        case 2:
            echo "The value is 2";
            break;
        case 3:
            echo "The value is 3";
            break;
        case 4:
            echo "The value is 4";
            break;
        case 5:
            header("Location:http://www.google.com");
            break;
        default:
            echo "Another";
            break;
    }
}

$x=1;
$y=20;

for($x;$x<=$y;$x++){
  if($x%2==0){
      echo $x." is Even <br>";
  }
    else{
        echo $x." is Odd <br>";
    }
}
$fruits=array("Apple","Orange","Watermealon","Banana","Pineapple");
foreach($fruits as $singleFruit){

    echo $singleFruit."<br>";
    if($singleFruit=="Watermealon"){
        break;
    }
    else{
        continue;
    }
}
function test_odd($var){

    return($var & 1);
}
$a1=array("a","b",2,3,4);
print_r(array_filter($a1,"test_odd"));

function odd($var)
{
    // returns whether the input integer is odd
    return($var & 1);
}

function even($var)
{
    // returns whether the input integer is even
    return(!($var & 1));
}

$array1 = array("a"=>1, "b"=>2, "c"=>3, "d"=>4, "e"=>5);
$array2 = array(6, 7, 8, 9, 10, 11, 12);

echo "Odd :\n";
print_r(array_filter($array1, "odd"));
echo "Even:\n";
print_r(array_filter($array2, "even"));

echo rand();
?>
</body>
</html>