<?php
 echo "This is PHP default Tag.<br>";
?>

<?
echo " This is PHP Short Open Tag.<br>";
?>

<script language="php">
    echo " This is PHP Short Open Tag.<br>";
</script>

<?php
echo 'This is single quoted string.<br>';
echo "This is single quoted string.<br>";
/*
$str=<<<EOD
<pre>
This is simpe heredoc example
for string literal
in PHP
</pre>
EOD;

echo $str;

$str=<<<'EOD'
<pre>
This is simpe nowdoc example
for string literal
in PHP
</pre>
EOD;

echo $str;

$cars=array("BMW","Toyota","Nissan");
echo $cars[2]." ".$cars[0];

var_dump($cars);
*/
//var_dump($GLOBALS);
echo $_SERVER['HTTP_HOST'];

$x=10;
$y=20;

function addition(){
 $GLOBALS['z']=$GLOBALS['x']+$GLOBALS['y'];
}

$yes=array('This','is','an array');

$result=is_array($yes);
echo "<br>".$result."<br>";

define('clc','Welcome to CLC');

echo __FILE__;
echo "<br>".__DIR__;

echo "<br>".clc;
var_dump($_SERVER);
?>